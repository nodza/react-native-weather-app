/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  MapView,
  Text,
  View,
} = React;

var Api = require('./src/api');

var Weather = React.createClass({
  getInitialState: function() {
    return {
      pin: {
        latitude: 0,
        longitude: 0
      },
      city: '',
      temperature: '',
      description: ''
    }
  },
  render: function() {
    var pins = [{
      latitude: 39,
      longitude: -91
    }];
    return <View style={styles.container}>
      <MapView
          annotations={[this.state.pin]}
          onRegionChangeComplete={this.onRegionChangeComplete}
          style={styles.map}>
      </MapView>
      <View style={styles.textWrapper}>
        <Text style={styles.title}>{this.state.city}</Text>
        <Text style={styles.text}>{this.state.temperature}</Text>
        <Text style={styles.text}>{this.state.description}</Text>
      </View>
    </View>
  },
  onRegionChangeComplete: function(region) {
    this.setState({
        pin: {
          latitude: region.latitude,
          longitude: region.longitude
        }
    });

    Api(region.latitude, region.longitude)
      .then((data) => {
        this.setState(data);
      });
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF'
  },
  map: {
    flex: 2,
    marginTop: 30
  },
  textWrapper: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 30
  },
  title: {
    fontSize: 30,
    color: 'purple'
  },
  text: {
    fontSize: 20,
    color: 'gray'
  }
});

AppRegistry.registerComponent('weather', () => Weather);
