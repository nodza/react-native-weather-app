/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var {
    AppRegistry,
    StyleSheet,
    MapView,
    View,
    } = React;

var Weather = React.createClass({
    render: function() {
        return <View>
                <MapView style={styles.map} />
            </View>
    }
});

var styles = StyleSheet.create({
    map: {
        flex: 1
    }
});

AppRegistry.registerComponent('weather', () => Weather);
